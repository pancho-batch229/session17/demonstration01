//console.log("Hi");

/*
	[SECTION] - Functions
	Functions in JS are lines/blocks of codes that tell our device/application to perform a specific task.
	Functions are mostly created to create complicated tasks to run several lines of code succession.
	Prevent repeating lines of codes.

	Function Declaration -> "function"

	Syntax:
		function functionName(){
		//code block
		let number = 10;
		console.log(number)
		}

	ERR0R console.log(number) -> local scope variable
*/

function printGrade(){
	let grade1 = 90;
	let grade2 = 95;
	let average = (grade1 + grade2)/2;

	console.log(average);
}

// calling or invoke
printGrade();

/*
	[SECTION] - Function Invocation
	functions can also be stored in variables - called function expression.
	anonymous functions have no names - a phantom expression.
*/

function declaredFunction() {
	console.log("Hello from declaredFunction()!")
}

declaredFunction();
printGrade();

let variableFunction = function(){
	console.log("you got this!");
}

variableFunction();

/*
function expression  function name
 |					  |
 V                    V
let variableName = function functionName(){
	body..
}
*/

let functionExpression = function functionName(){
	console.log("baby come back!");
}

functionExpression();	//correct invocation
// functionName(); incorrect invocation 

declaredFunction();
declaredFunction = function(){
	console.log("updated declaredFunction()!");
}

declaredFunction();

functionExpression();
functionExpression();

functionExpression = function(){
	console.log("I just can't live without you!");
}

functionExpression();

// however, reassigning of function expression initialized with const is no go

const constantFunction = function(){
	console.log("initialized const :D");
}

constantFunction();

/*
ERR0R
const constantFunction = function(){
	console.log("initialized const :D");
}

constantFunction();*/

/*
	[SECTION] - Function Scoping
	
	1. Local/block Scope
	2. Global Scope
	3. Function
*/

{
	let localVariable = "Manny Rivera";
	console.log(localVariable);
}

let globalVariable = "El Tigre!";

console.log(globalVariable);
// ERR0R console.log(localVariable);

function showNames(){
	// function scoped variables
	var functionVar = "Joe";
	const functionConst = "Mama";
	let functionLet = "lol";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}

showNames();

/*
ERR0R
console.log(functionVar);
console.log(functionConst);
console.log(functionLet);*/

function myNewFunction(){
	let name = "Fritz Smith";

	function nestedFunction(){
		let nestedName = "Mike Schmidt";
		console.log(name);
		console.log(nestedName);
	}
	nestedFunction();
	// ERR0R console.log(nestedName); -> function scoped
}

myNewFunction();
// ERR0R nestedFunction();

let globalName = "Alejandro";

function myNewFunction2(){
	let nameInside = "Heather";

	console.log(globalName);
}

myNewFunction2();

// ERR0R console.log(nameInside);

/*
	[SECTION] - Using Alerts
	alert() allows to show small windows atop the browser.
*/

// alert("seGAAAAAAAAAH");

function showAlert(){
	alert("seGAAAAAAAAAH");
}

/*
	// COMMENTED EXAMPLE 
	showAlert(); had to comment

	console.log("Alert dismissed!");
*/

/*
	Notes on alerts
	shows only alert() for short dialogue message.
	do not abuse with long messages.
*/

/*
	[SECTION] - Using Prompts
	prompt() allows to show small windows to gather input.
	stored in variables.
*/

/*
	// COMMENTED EXAMPLE 
  	let showPrompt = prompt("Enter your name...");

  	console.log("Hello, " + showPrompt);
  	console.log(typeof showPrompt);

-----------------------------------------------------------------

	let sampleNullPrompt = prompt("Do not input anything!");

	console.log(sampleNullPrompt);

function printWelcomeMessage(){
	let firstName = prompt("Enter given name..");
	let lastName = prompt("Enter surname..");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("welcome to the page!");
}

printWelcomeMessage();
*/

/*
	[SECTION] - Function Naming Convention 
	function names should be definitive of the given performance task.
	usually contains a verb.
*/

// GOOD PRACTICE OF NAMING CONVETION
function getCourse(){
	let courses = ["Science 101", "Mathematics", "English Made Easy"];
	console.log(courses);
}

getCourse();

// avoid generic names and use specifics to what the code does

/*
	// BAD PRACTICE OF NAMING CONVETION
	function get(){
		let name = "Dumb";
		console.log(name);
	}

	get();
*/

